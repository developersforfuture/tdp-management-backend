const mongoose = require('../../common/services/mongoose.service').mongoose;
const Schema = mongoose.Schema;

const countrySchema = new Schema({
    name: {type: String, required: true, unique: true},
    short_name: {type: String, required: true, unique: true},
    createdAt: {type: Date, default: Date.now},
}, {versionKey: false});

countrySchema.virtual('id').get(function() {
    return this._id.toHexString();
});

// Ensure virtual fields are serialised.
countrySchema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: function(doc, ret) {
        delete ret._id;
        delete ret.hash;
    },
});
countrySchema.pre('save', (next) => {
    now = new Date();
    if (!this.createdAt) {
        this.createdAt = now;
    }
    return next();
});
countrySchema.findById = function(cb) {
    return this.model('Country').find({id: this.id}, cb);
};

// Exports the country for use elsewhere.
module.exports = mongoose.model('Country', countrySchema);
