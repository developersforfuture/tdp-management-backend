const VerifyUserMiddleware = require('./middlewares/verify.user.middleware');
const AuthorizationController = require('./controllers/authorization.controller');
const AuthValidationMiddleware = require('../common/middlewares/auth.validation.middleware');
exports.routesConfig = function(app) {
    app.post('/verify', [AuthorizationController.verifyRegistration]);
    app.post('/login_check', [
        VerifyUserMiddleware.hasAuthValidFields,
        VerifyUserMiddleware.isPasswordAndUserMatch,
        AuthorizationController.login,
    ]);

    app.post('/auth/refresh', [
        AuthValidationMiddleware.validJWTNeeded,
        AuthValidationMiddleware.verifyRefreshBodyField,
        AuthValidationMiddleware.validRefreshNeeded,
        AuthorizationController.login,
    ]);

    app.post('/register', [
        VerifyUserMiddleware.hasRegistrationValidFields,
        VerifyUserMiddleware.hasNoUsedUserNameAndEmail,
        AuthorizationController.register,
    ]);
};
