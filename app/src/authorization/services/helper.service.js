
const crypto = require('crypto');

exports.genRandomString = (length) => {
    return crypto.randomBytes(Math.ceil(length/2))
        .toString('hex') /** convert to hexadecimal format */
        .slice(0, length); /** return required number of characters */
};
exports.sha512 = function(password, salt = null) {
    salt = salt || this.genRandomString(16);
    return crypto.createHmac('sha512', salt).update(password).digest('hex');
};

exports.saltHashPassword = function(userpassword) {
    const salt = this.genRandomString(16); /** Gives us salt of length 16 */
    const passwordDataHash = this.sha512(userpassword, salt);
    return salt + '$' + passwordDataHash;
};
