// During the test the env variable is set to test
process.env.NODE_ENV = 'test';

const mongoose = require('mongoose');
const Country = require('../../data/models/country');

// Require the dev-dependencies
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../../index');
const should = chai.should();

chai.use(chaiHttp);
describe('Countries', () => {
    beforeEach((done) => {
        Country.deleteMany({}, (err) => {
            if (err) throw Error(err);
            done();
        });
    });

    describe('GET /data/countries (list countries)', () => {
        beforeEach((done) => {
            const orga = new Country({name: 'Test Country', short_name: 'tc'});
            orga.save((err, o) => {
                if (err) throw Error(err);
                done();
            });
        });

        it('should GET one country, when one is builded', (done) =>{
            chai.request(server)
                .get('/data/countries')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.be.eql(1);
                    done();
                });
        });
    });
});
