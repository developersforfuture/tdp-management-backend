const process = require('process');

module.exports = {
    'port': process.env.APP_PORT || 3600,
    'appEndpoint': 'http://localhost:3600',
    'apiEndpoint': 'http://localhost:3600',
    'jwt_secret': process.env.JWT_SECRET || 'myS33!!creeeT',
    'jwt_expiration_in_seconds': 36000,
    'env': process.env.NODE_ENV || 'dev',
    'permissionLevels': {
        'NORMAL_USER': 1,
        'ADMIN': 2048,
    },
    'dbHost': process.env.DB_HOST || '0.0.0.0',
    'dbPort': process.env.DB_PORT || 27017,
    'dbDatabase': process.env.DB_DATABASE || 'tourdeplanet',
    'dbUser': process.env.DB_USER || 'tourdeplanet',
    'dbPassword': process.env.DB_PASSWORD || 'tourdeplanet',
};
