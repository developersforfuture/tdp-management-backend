const permissionMiddleware = require(
    '../common/middlewares/auth.permission.middleware'
);
const validationMiddleware = require(
    '../common/middlewares/auth.validation.middleware'
);
const config = require('../common/config/env.config');
const tourController = require('./controller/tour.controller');
const tourDataValidationMiddleWare = require('./middleware/tour.middleware');

exports.routesConfig = function(app) {
    app.get('/users/:userId/tours/:tourId', [
        validationMiddleware.validJWTNeeded,
        permissionMiddleware.userStillEnabled,
        permissionMiddleware
            .minimumPermissionLevelRequired(
                config.permissionLevels.NORMAL_USER,
            ),
        tourController.getTour,
    ]);
    app.get('/users/:userId/tours', [
        validationMiddleware.validJWTNeeded,
        permissionMiddleware.userStillEnabled,
        permissionMiddleware
            .minimumPermissionLevelRequired(
                config.permissionLevels.NORMAL_USER,
            ),
        tourController.getTours,
    ]);

    app.patch('/users/:userId/tours/:tourId', [
        validationMiddleware.validJWTNeeded,
        permissionMiddleware.userStillEnabled,
        permissionMiddleware
            .minimumPermissionLevelRequired(
                config.permissionLevels.NORMAL_USER,
            ),
        tourDataValidationMiddleWare.hasAcceptablePatchData,
        tourController.patchTour,
    ]);
    app.post('/users/:userId/tours', [
        validationMiddleware.validJWTNeeded,
        permissionMiddleware.userStillEnabled,
        permissionMiddleware
            .minimumPermissionLevelRequired(
                config.permissionLevels.NORMAL_USER,
            ),
        tourDataValidationMiddleWare.hasValidTourDataForCreation,
        tourController.createTour,
    ]);
};
