/* eslint-disable no-invalid-this */
process.env.NODE_ENV = 'test';

const User = require('../../users/users.model');
const Organisation = require('./../../data/models/organisation');
const Country = require('./../../data/models/organisation');
const MeansOfTransportation = require('./../../data/models/meansoftransportation');

const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../../index');

chai.use(chaiHttp);

describe('Attendee data', () => {
    before(async () => {
        User.deleteMany({}, (err) => {
            if (err) throw (err);
        });
        await chai.request(server)
            .post('/register')
            .set('Content-Type', 'application/json')
            .send({
                username: 'testuser',
                email: 'test@tdp.de',
                plainPassword: 'pAssW0rd!',
                termsAccepted: true,
            });
        const res = await chai.request(server)
            .post('/login_check')
            .set('Content-Type', 'application/json')
            .send({
                username: 'testuser',
                plainPassword: 'pAssW0rd!',
            });
        this.token = res.body.accessToken;
        this.userId = res.body.userValues.userId;
        const organisation = new Organisation({name: 'Test-Orga', short_name: 'short'});
        const firstOrga = await organisation.save();
        this.firstOrgaId = firstOrga.id;
        const country = new Country({name: 'Test-Country', short_name: 'tc'});
        const firstCountry = await country.save();
        this.firstCountryId = firstCountry.id;
        const meanOfTransportation = new MeansOfTransportation({name: 'Bike', iconClass: 'tdp_test', color: 'blue'});
        const firstMeanOfTransportation = await meanOfTransportation.save();
        this.firstMeanOfTransportationId = firstMeanOfTransportation.id;
    });

    describe('Attendee Profile', () => {
        it('should serve users attendee profile data', async () => {
            const res = await chai.request(server)
                .get('/users/' + this.userId + '/attendee-profile')
                .set('authorization', 'Bearer ' + this.token)
                .set('Content-Type', 'application/json');
            res.should.have.status(200);
            res.body.should.have.property('firstName');
            res.body.should.have.property('secondName');
            res.body.should.have.property('username');
            res.body.should.have.property('mail');
            res.body.should.have.property('organisation');
            res.body.should.have.property('country');
        });

        it('should serve users attendee tour profile data', async () => {
            const res = await chai.request(server)
                .get('/users/' + this.userId + '/attendee-tour-profile')
                .set('authorization', 'Bearer ' + this.token)
                .set('Content-Type', 'application/json');
            res.should.have.status(200);
            res.body.should.have.property('isBuildOwnTour');
            res.body.should.have.property('isOrga');
            res.body.should.have.property('isPress');
            res.body.should.have.property('isJoinOtherTour');
            res.body.should.have.property('daysToParticipate');
            res.body.should.have.property('distanceToParticipate');
            res.body.should.have.property('unit');
            res.body.should.have.property('home');
        });

        it('should update attendee profile', async () => {
            const res = await chai.request(server)
                .patch('/users/' + this.userId + '/attendee-profile')
                .set('authorization', 'Bearer ' + this.token)
                .set('Content-Type', 'application/json')
                .send({
                    firstName: 'max',
                    secondName: 'Tester',
                    organisation: this.firstOrgaId,
                    country: this.firstCountryId,
                    username: 'newtestuser',
                    mail: 'max@tdp.de',
                });
            res.should.have.status(200);
            res.body.should.have.property('firstName');
            res.body.firstName.should.be.equal('max');
            res.body.should.have.property('secondName');
            res.body.secondName.should.be.equal('Tester');
            res.body.should.have.property('username');
            res.body.username.should.be.equal('newtestuser');
            res.body.should.have.property('mail');
            res.body.mail.should.be.equal('max@tdp.de');
            res.body.should.have.property('organisation');
            res.body.organisation.should.be.equal(this.firstOrgaId);
            res.body.should.have.property('country');
            res.body.country.should.be.equal(this.firstCountryId);
        });

        it('should update attendee tour profile', async () => {
            const res = await chai.request(server)
                .patch('/users/' + this.userId + '/attendee-tour-profile')
                .set('authorization', 'Bearer ' + this.token)
                .set('Content-Type', 'application/json')
                .send({
                    isBuildOwnTour: true,
                    isOrga: true,
                    isPress: true,
                    isJoinOtherTour: true,
                    home: {
                        longitude: 0,
                        latitude: 0,
                        name: 'testname',
                    },
                    daysToParticipate: 10,
                    distanceToParticipate: 12,
                    meanOfTransportation: this.firstMeanOfTransportationId,
                    unit: 'm',
                });
            res.should.have.status(200);
            res.body.should.have.property('isBuildOwnTour');
            res.body.isBuildOwnTour.should.be.equal(true);
            res.body.should.have.property('isOrga');
            res.body.should.have.property('isPress');
            res.body.should.have.property('isJoinOtherTour');
            res.body.should.have.property('daysToParticipate');
            res.body.should.have.property('distanceToParticipate');
            res.body.should.have.property('unit');
            res.body.unit.should.be.equal('m');
            res.body.should.have.property('home');
            res.body.should.have.property('meanOfTransportation');
            res.body.meanOfTransportation.should.be.equal(this.firstMeanOfTransportationId);
        });
    });

    describe('Attendee Donations', async () => {
        it('should return empty list, when nothing was added', async () => {
            const res = await chai.request(server)
                .get('/users/' + this.userId + '/attendee-donations')
                .set('authorization', 'Bearer ' + this.token)
                .set('Content-Type', 'application/json');
            res.should.have.status(200);
            res.body.length.should.be.equal(0);
        });
        it('should return list with new, when adding valid', async () => {
            const res = await chai.request(server)
                .post('/users/' + this.userId + '/attendee-donations')
                .set('authorization', 'Bearer ' + this.token)
                .set('Content-Type', 'application/json')
                .send({
                    point: {longitude: 10, latitude: 20, name: 'test point'},
                    date: '27/05/2020',
                    by: this.firstMeanOfTransportationId,
                    unit: 'km',
                    amount: 15,
                });
            res.should.have.status(201);
            res.body.length.should.be.equal(1);
            res.body[0].should.have.property('point');
        });
    });
});
