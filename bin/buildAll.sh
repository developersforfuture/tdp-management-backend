#!/usr/bin/env sh

set -ex

VERSION_TAG=${VERSION_TAG-'0.0.1'}
DOCKER_IMAGE=${DOCKER_IMAGE-'registry.gitlab.com/developersforfuture/tdp-management-backend'}

docker pull $DOCKER_IMAGE/production:$VERSION_TAG || true

docker build ./ -t $DOCKER_IMAGE/production:$VERSION_TAG --cache-from $DOCKER_IMAGE/production:$VERSION_TAG

docker push $DOCKER_IMAGE/production:$VERSION_TAG