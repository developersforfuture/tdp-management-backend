const mongoose = require('../../common/services/mongoose.service').mongoose;
const Schema = mongoose.Schema;

const tourOrganizerSchema = new Schema({
    name: {type: String, required: true, unique: true},
    type: {type: String, required: true},
    createdAt: {type: Date, default: Date.now},
}, {versionKey: false});

tourOrganizerSchema.virtual('id').get(function() {
    return this._id.toHexString();
});

// Ensure virtual fields are serialised.
tourOrganizerSchema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: function(doc, ret) {
        delete ret._id;
        delete ret.hash;
    },
});
tourOrganizerSchema.pre('save', (next) => {
    now = new Date();
    if (!this.createdAt) {
        this.createdAt = now;
    }
    return next();
});
tourOrganizerSchema.findById = function(cb) {
    return this.model('TourOrganizer').find({id: this.id}, cb);
};

// Exports the country for use elsewhere.
module.exports.Model = mongoose.model('TourOrganizer', tourOrganizerSchema);
module.exports.Schema = tourOrganizerSchema;
