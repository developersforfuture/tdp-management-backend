const logger = require('./../../logger');
const Assert = require('../../common/services/assert.helper').Assert;

exports.hasValidTourDataForCreation = (req, res, next) => {
    if (!Assert.hasValidKey(req, 'body')) {
        logger.warn('No body found');
        return res.status(400).send('No data found');
    }

    const tourData = {};
    if (!Assert.hasValidKey(req.body, 'title')) {
        logger.warn('Tour title not found');
        return res.status(400).send('Tour title not valid');
    }
    tourData.title = req.body.title;
    if (!Assert.hasValidKey(req.body, 'organizer')) {
        logger.warn('Tour organizer not found');
        return res.status(400).send('Tour organizer not valid');

    }
    tourData.organizer = req.body.organizer;

    if (!Assert.hasValidKey(req.body, 'start')) {
        logger.warn('Tour end not found');
        return res.status(400).send('Tour start point not valid');
    }
    if (!Assert.hasValidKey(req.body.start, 'name')) {
        logger.warn('Tour start has no name');
        return res.status(400).send('Tour start point not valid');
    }
    if (!Assert.hasValidKey(req.body.start, 'longitude')) {
        logger.warn('Tour start has no longitude');
        return res.status(400).send('Tour start point not valid');
    }
    if (!Assert.hasValidKey(req.body.start, 'latitude')) {
        logger.warn('Tour start has no latitude');
        return res.status(400).send('Tour start point not valid');
    }
    if (!Assert.hasValidKey(req.body.start, 'date')) {
        logger.warn('Tour start has no date');
        return res.status(400).send('Tour start point not valid');
    }
    tourData.start = req.body.start;

    if (!Assert.hasValidKey(req.body, 'end')) {
        logger.warn('Tour end not found');
        return res.status(400).send('Tour end point not valid');
    }
    if (!Assert.hasValidKey(req.body.end, 'name')) {
        logger.warn('Tour end has no name');
        return res.status(400).send('Tour end point not valid');
    }
    if (!Assert.hasValidKey(req.body.end, 'longitude')) {
        logger.warn('Tour end has no longitude');
        return res.status(400).send('Tour end point not valid');
    }
    if (!Assert.hasValidKey(req.body.end, 'latitude')) {
        logger.warn('Tour end has no latitude');
        return res.status(400).send('Tour end point not valid');
    }
    if (!Assert.hasValidKey(req.body.end, 'date')) {
        logger.warn('Tour end has no date');
        return res.status(400).send('Tour end point not valid');
    }
    if (Assert.hasValidKey(req.body, 'description')) {
        tourData.description = req.body.description;
    }
    tourData.end = req.body.end;
    req.body.tourData = tourData;

    return next();
};

/**
 * We do accept values for base properties here like:
 * - id, which should be the same like in the url
 * - organizer
 * - start + end
 * - title
 *
 * To edit segment data, we use a separate route.
 *
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
exports.hasAcceptablePatchData = (req, res, next) => {
    if (!req.body) {
        logger.warn('No body found');
        return res.status(400).send('No data found');
    }

    const tourData = {};
    if (!req.body.id) {
        logger.warn('No tour id send on patch request');
        return res.status(400).send();
    }

    if (req.body.id !== req.params['tourId']) {
        logger.warn('Tour id does not match');
        return res.status(400).send();
    }

    tourData.id = req.body.id;
    if (Assert.hasValidKey(req.body, 'title')) {
        tourData.title = req.body.title;
    }
    if (Assert.hasValidKey(req.body, 'organizer')) {
        tourData.organizer = req.body.organizer;
    }

    if (Assert.hasValidKey(req.body, 'start')) {
        if (!Assert.hasValidKey(req.body.start, 'name')) {
            logger.warn('Tour start has no name');
            return res.status(400).send();
        }
        if (!Assert.hasValidKey(req.body.start, 'longitude')) {
            logger.warn('Tour start has no longitude');
            return res.status(400).send();
        }
        if (!Assert.hasValidKey(req.body.start, 'latitude')) {
            logger.warn('Tour start has no latitude');
            return res.status(400).send();
        }
        if (!Assert.hasValidKey(req.body.start, 'date')) {
            logger.warn('Tour start has no date');
            return res.status(400).send();
        }
        tourData.start = req.body.start;
    } else {
        logger.warn('Segment has no start at all');
        return res.status(400).send();
    }

    if (Assert.hasValidKey(req.body, 'end')) {
        if (!Assert.hasValidKey(req.body.end, 'name')) {
            logger.warn('Tour start has no name');
            return res.status(400).send();
        }
        if (!Assert.hasValidKey(req.body.end, 'longitude')) {
            logger.warn('Tour start has no longitude');
            return res.status(400).send();
        }
        if (!Assert.hasValidKey(req.body.end, 'latitude')) {
            logger.warn('Tour start has no latitude');
            return res.status(400).send();
        }
        if (!Assert.hasValidKey(req.body.end, 'date')) {
            logger.warn('Tour start has no date');
            return res.status(400).send();
        }
        tourData.end = req.body.end;
    }  else {
        logger.warn('Segment has no end at all');
        return res.status(400).send();
    }

    if (Assert.hasValidKey(req.body, 'description')) {
        tourData.description = req.body.description;
    }
    req.body.tourData = tourData;

    return next();
};


exports.tourSegmentHasValidData = (req, res, next) => {
    if (!req.body) {
        logger.warn('No data found on tour segment request.');
        return res.status(400).send('No data found');
    }

    const segmentData = {};
    if (Assert.hasValidKey(req.body, 'start')) {
        if (!Assert.hasValidKey(req.body.start, 'name')) {
            logger.warn('Segment start has no name');
            return res.status(400).send();
        }
        if (!Assert.hasValidKey(req.body.start, 'longitude')) {
            logger.warn('Segment start has no longitude');
            return res.status(400).send();
        }
        if (!Assert.hasValidKey(req.body.start, 'latitude')) {
            logger.warn('Segment start has no latitude');
            return res.status(400).send();
        }
        if (!Assert.hasValidKey(req.body.start, 'date')) {
            logger.warn('Segment start has no date');
            return res.status(400).send();
        }
        segmentData.start = req.body.start;
    }

    if (Assert.hasValidKey(req.body, 'end')) {
        if (!Assert.hasValidKey(req.body.end, 'name')) {
            logger.warn('Segment start has no name');
            return res.status(400).send();
        }
        if (!Assert.hasValidKey(req.body.end, 'longitude')) {
            logger.warn('Segment start has no longitude');
            return res.status(400).send();
        }
        if (!Assert.hasValidKey(req.body.end, 'latitude')) {
            logger.warn('Segment start has no latitude');
            return res.status(400).send();
        }
        if (!Assert.hasValidKey(req.body.end, 'date')) {
            logger.warn('Segment start has no date');
            return res.status(400).send();
        }
        segmentData.end = req.body.end;
    }

    if (!Assert.hasValidKey(req.body,'name')) {
        logger.warn('No name data found on tour segment data.');
        return res.status(400).send('No name found.');
    }
    segmentData.name = req.body.name;

    if (Assert.hasValidKey(req.body,'id') && Assert.hasValidKey(req.params,'segmentId') && req.body.id !== req.params['segmentId']) {
        logger.warn('id does not match.');
        return res.status(400).send();
    }

    req.body.segementData = segmentData;

    return next();
};
