process.env.NODE_ENV = 'test';

const User = require('../../users/users.model');

const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../../index');

chai.use(chaiHttp);
describe('Authentication', () => {
    describe('Register User', () => {
        beforeEach((done) => {
            User.deleteMany({}, (err) => {
                if (err) throw (err);
                done();
            }).catch((err) => {
                done();
            });
        });
        beforeEach((done) => {
            User
                .create({
                    username: 'testuser1',
                    email: 'test1@tdp.de',
                    password: 'password1',
                    terms_accepted: true,
                })
                .then((error, user) => {
                    if (error) throw (error);
                    done();
                }).catch((err) => {
                    done();
                });
        });
        it('should return raw user data only', (done) =>{
            chai.request(server)
                .post('/register')
                .set('Content-Type', 'application/json')
                .send({
                    username: 'testuser',
                    email: 'test@tdp.de',
                    plainPassword: 'pAssW0rd!',
                    termsAccepted: true,
                })
                .end((err, res) => {
                    res.should.have.status(201);
                    const user = res.body;
                    user.should.have.property('attendee');
                    user.should.have.property('username');
                    user.should.not.have.property('password');
                    done();
                });
        });

        it('should block same username', (done) =>{
            chai.request(server)
                .post('/register')
                .send({username: 'testuser1', email: 'test@tdp.de', plainPassword: 'pAssW0rd!', termsAccepted: true})
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.have.key('error');
                    res.body.error.should.contain('exists');
                    done();
                });
        });

        it('should block same email', (done) =>{
            chai.request(server)
                .post('/register')
                .send({username: 'testuser', email: 'test1@tdp.de', plainPassword: 'pAssW0rd!', termsAccepted: true})
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.have.key('error');
                    res.body.error.should.contain('exists');
                    done();
                });
        });
    });

    describe('Login a User', () => {
        beforeEach((done) => {
            User.deleteMany({}, (err) => {
                if (err) throw (err);
                done();
            }).catch((err) => {
                done();
            });
        });
        beforeEach((done) => {
            chai.request(server)
                .post('/register')
                .set('Content-Type', 'application/json')
                .send({
                    username: 'testuser',
                    email: 'test@tdp.de',
                    plainPassword: 'pAssW0rd!',
                    termsAccepted: true,
                })
                .end((err, res) => {
                    done();
                });
        });

        it('should login with username', (done) => {
            chai.request(server)
                .post('/login_check')
                .set('Content-Type', 'application/json')
                .send({
                    username: 'testuser',
                    plainPassword: 'pAssW0rd!',
                })
                .end((err, res) => {
                    res.should.have.status(201);
                    const token = res.body;
                    token.should.have.property('accessToken');
                    token.should.have.property('refreshToken');
                    token.should.have.property('userId');
                    done();
                });
        });

        it('should login with mail address', (done) => {
            chai.request(server)
                .post('/login_check')
                .set('Content-Type', 'application/json')
                .send({
                    username: 'test@tdp.de',
                    plainPassword: 'pAssW0rd!',
                })
                .end((err, res) => {
                    res.should.have.status(201);
                    const token = res.body;
                    token.should.have.property('accessToken');
                    token.should.have.property('refreshToken');
                    token.should.have.property('userId');
                    done();
                });
        });

        it('should not login with non existing username', (done) => {
            chai.request(server)
                .post('/login_check')
                .set('Content-Type', 'application/json')
                .send({
                    username: 'test',
                    plainPassword: 'pAssW0rd!',
                })
                .end((err, res) => {
                    res.should.have.status(404);
                    done();
                });
        });
    });
});
