
const assert = {
    hasValidKey(object, key) {
        return object.hasOwnProperty(key) && object[key];
    }
};
exports.Assert = assert;
