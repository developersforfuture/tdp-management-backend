const chai = require('chai');
const server = require('../index');

exports.registerUser = async  function (userName, password, email) {
    const result = await chai.request(server)
        .post('/register')
        .set('Content-Type', 'application/json')
        .send({
            username: userName,
            email: email,
            plainPassword: password,
            termsAccepted: true,
        });
    const res = await chai.request(server)
        .post('/login_check')
        .set('Content-Type', 'application/json')
        .send({
            username: userName,
            plainPassword: password,
        });
    const token = res.body.accessToken;
    const userId = res.body.userValues.userId;

    return {token, userId};
};
