const permissionMiddleware = require(
    '../common/middlewares/auth.permission.middleware',
);
const validationMiddleware = require(
    '../common/middlewares/auth.validation.middleware',
);
const UserController = require('./users.controller');

exports.routesConfig = function(app) {
    app.get('/users', [
        validationMiddleware.validJWTNeeded,
        permissionMiddleware.onlySameUserOrAdminCanDoThisAction,
        UserController.getAll,
    ]);
    app.get('/users/:userId', [
        validationMiddleware.validJWTNeeded,
        permissionMiddleware.onlySameUserOrAdminCanDoThisAction,
        UserController.get,
    ]);
    app.patch('/users/:userId', [
        validationMiddleware.validJWTNeeded,
        permissionMiddleware.onlySameUserOrAdminCanDoThisAction,
        UserController.update,
    ]);
    app.delete('/users/:userId', [
        validationMiddleware.validJWTNeeded,
        permissionMiddleware.onlySameUserOrAdminCanDoThisAction,
        UserController._delete,
    ]);
};
