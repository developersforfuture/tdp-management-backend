const meanOfTransportationModel = require('./../data/models/meansoftransportation');

exports.hasRequiredFieldsForAddingDonation = (req, res, next) => {
    if (!req.body) return res.status(400).send();

    ['by', 'date', 'point', 'unit', 'amount'].forEach((fieldName) => {
        if (!req.body.hasOwnProperty(fieldName)) {
            return res.status(400).send('Missing field "' + fieldName + '".');
        }
    });
    const mot = meanOfTransportationModel.findById(req.body.by);
    if (!mot) {
        return res
            .status(400)
            .send('No valid Mean of Transportation found for id: "' + req.body.by + '".');
    }

    return next();
};
