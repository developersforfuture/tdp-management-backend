exports.validTour = {
    title: 'Tour',
    organizer: {
        name: 'Tour Organizer',
        type: 'single'
    },
    start: {
        name: 'Start',
        longitude: 0.1,
        latitude: 0.1,
        date: '09/07/2020'
    },
    end: {
        name: 'End',
        longitude: 0.2,
        latitude: 0.2,
        date: '10/07/2020',
    },
    description: 'some description'
};