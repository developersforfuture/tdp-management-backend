/* eslint-disable no-invalid-this */
const userService = require('../users/users.service');
const helper = require('./../authorization/services/helper.service');
const attendeeDonation = require('./attendee_donation.model');
const AttendeeService = require('./attendee.service');
const meanOfTransportation = require('./../data/models/meansoftransportation');

exports.updateAttendeeTourProfile = (req, res, next) => {
    userService.patchUserAttendeeTourProfile(req.params.userId, req.body)
        .then(() => {
            return this.getAttendeeTourProfile(req, res, next);
        })
        .catch((err) => next(err));
};

exports.getAttendeeTourProfile = (req, res, next) => {
    userService.findById(req.params.userId)
        .then((result) => {
            return res.status(200).send({
                isBuildOwnTour: result.attendee.isBuildOwnTour,
                isJoinOtherTour: result.attendee.isJoinOtherTour,
                isPress: result.attendee.isPress,
                isOrga: result.attendee.isOrga,
                home: result.attendee.home,
                daysToParticipate: result.attendee.daysToParticipate,
                distanceToParticipate: result.attendee.distanceToParticipate,
                meanOfTransportation: result.attendee
                    .hasOwnProperty('meanOfTransportation') ?
                    result.attendee.meanOfTransportation :
                    null,
                unit: result.attendee.unit,
            });
        })
        .catch((err) => next(err));
};

exports.updateAttendeeProfile = (req, res, next) => {
    userService.patchUserAttendeeProfile(req.params.userId, req.body)
        .then(() => {
            return this.getAttendeeProfile(req, res, next);
        })
        .catch((err) => next(err));
};
exports.getAttendeeProfile = (req, res, next) => {
    userService.findById(req.params.userId)
        .then((result) => {
            return res.status(200).send({
                username: result.username,
                mail: result.email,
                firstName: result.attendee.firstName,
                secondName: result.attendee.secondName,
                organisation: result.attendee.hasOwnProperty('organisation') ?
                    result.attendee.organisation :
                    null,
                country: result.attendee.hasOwnProperty('country') ?
                    result.attendee.country :
                    null,
            });
        })
        .catch((err) => next(err));
};
exports.updateAttendeePassword = (req, res, next) => {
    if (req.body.plainPassword) {
        req.body.password = helper.saltHashPassword(req.body.plainPassword);
    }
    userService.update(req.body)
        .then((result) => {
            return res.status(204).send();
        })
        .catch((err) => next(err));
};

exports.addAttendeeDonation = async (req, res, next) => {
    userService
        .findById(req.params.userId)
        .then((user) => {
            if (!user.attendee.hasOwnProperty('donations')) {
                user.attendee.donations = [];
            }
            const donation = new attendeeDonation.Model(req.body);
            const mot = meanOfTransportation.findOne({id: donation.by});
            if (!mot) {
                return res.status(400).send('Mean of Transportation is not valid');
            }
            donation.save().then((donation) => {
                if (!user.attendee.hasOwnProperty('donations')) {
                    user.attendee.donations = [];
                }
                user.attendee.donations.push(donation);
                return userService.patchUserAttendeeDonations(user.id, user.attendee)
                    .then((result) => {
                        AttendeeService.findDonations(req.params.userId)
                            .then((result) => res.status(201).send(result))
                            .catch((err) => next(err));
                    })
                    .catch((err) => next(err));
            }).catch((err) => next(err));
        })
        .catch((err) => next(err));
};

exports.getAttendeeDonations = async (req, res, next) => {
    AttendeeService.findDonations(req.params.userId)
        .then((result) => res.status(200).send(result))
        .catch((err) => next(err));
};

