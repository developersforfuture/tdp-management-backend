FROM node:13

WORKDIR /app/src/
# install and cache app dependencies
COPY /app/src /app/src

# add `/app/src/node_modules/.bin` to $PATH
ENV PATH /app/src/node_modules/.bin:$PATH

# install and cache app dependencies
RUN npm install

EXPOSE 3600
CMD [ "node", "index.js" ]
ARG version_tag
ENV VERSION_TAG $version_tag
LABEL image.name=tdp-backend \
      image.version=${version_tag}} \
      image.tag=registry.gitlab.com/developersforfuture/tdp-tourmanagement-backend/tdp-backend/development \
      image.scm.commit=$commit \
      image.scm.url=git@gitlab.com:DevelopersForFuture/tdp-tourmanagement-backend.git \
      image.author="Maximilian Berghoff <maximilian.berghoff@gmx.de>"
