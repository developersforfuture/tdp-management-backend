// During the test the env variable is set to test
process.env.NODE_ENV = 'test';

const mongoose = require('mongoose');
const MeanOfTransportation = require('../../data/models/meansoftransportation');

// Require the dev-dependencies
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../../index');
const should = chai.should();

chai.use(chaiHttp);
describe('MeanOfTransportation', () => {
    beforeEach((done) => {
        MeanOfTransportation.deleteMany({}, (err) => {
            if (err) throw Error(err);
            done();
        });
    });

    describe('GET /data/meansoftransportation (list meansoftransportation)', () => {
        beforeEach((done) => {
            const orga = new MeanOfTransportation({name: 'Test Orga', iconClass: 'ti', color: 'blue'});
            orga.save((err, o) => {
                if (err) throw Error(err);
                done();
            });
        });

        it('should GET one organisation, when one is builded', (done) =>{
            chai.request(server)
                .get('/data/meansoftransportation')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.be.eql(1);
                    done();
                });
        });
    });
});
