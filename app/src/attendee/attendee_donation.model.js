/* eslint-disable no-invalid-this */
const mongoose = require('../common/services/mongoose.service').mongoose;
const Schema = mongoose.Schema;

const attendeeDonationSchema = new Schema({
    point: {type: Object, default: {longitude: 0, latitude: 0, name: ''}},
    date: {type: String, required: true},
    amount: {type: Number, required: true, default: 0},
    by: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'MeanOfTransportation',
        required: true,
    },
    unit: {type: String, required: true, default: 'km'},
    createdAt: {type: Date, default: Date.now},
});
attendeeDonationSchema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: function(doc, ret) {
        delete ret._id;
        delete ret.hash;
    },
});
attendeeDonationSchema.pre('save', (next) => {
    now = new Date();
    if (!this.createdAt) {
        this.createdAt = now;
    }
    return next();
});
exports.schema = attendeeDonationSchema;
exports.Model = mongoose.model('AttendeeDonation', attendeeDonationSchema);
