const mongoose = require('mongoose');
const config = require('./../config/env.config');
const logger = require('./../../logger');
let count = require('./../config/env.config');

const options = {
    autoIndex: false, // Don't build indexes
    // reconnectTries: 30, // Retry up to 30 times
    // reconnectInterval: 500, // Reconnect every 500ms
    poolSize: 10, // Maintain up to 10 socket connections
    // If not connected, return errors
    // immediately rather than waiting for reconnect
    bufferMaxEntries: 0,
    // geting rid off the depreciation errors
    useNewUrlParser: true,
    useUnifiedTopology: true,
};
let dbUrl = '';
if (process.env.NODE_ENV === 'dev') {
    dbUrl = `mongodb://${config.dbUser}:${config.dbPassword}@${config.dbHost}:${config.dbPort}/${config.dbDatabase}`;
} else if (process.env.NODE_ENV === 'test') {
    dbUrl = `mongodb://${config.dbUser}:${config.dbPassword}@${config.dbHost}:${config.dbPort}/${config.dbDatabase}_test`;
} else {
    dbUrl = `mongodb://${config.dbUser}:${config.dbPassword}@${config.dbHost}:${config.dbPort}/${config.dbDatabase}`;
}
logger.info('Connection with: ' + dbUrl);
const connectWithRetry = () => {
    logger.info('MongoDB connection with retry');
    mongoose.connect(dbUrl, options).then(() => {
        logger.info('MongoDB is connected');
    }).catch((err) => {
        logger.warn('MongoDB connection unsuccessful, retry after 5 seconds. ', ++count);
        logger.warn(err);
        setTimeout(connectWithRetry, 5000);
    });
};

connectWithRetry();

exports.mongoose = mongoose;