const userModel = require('./../users/users.model');

exports.findAttendeeByUserId = (userId) => {
    return new Promise((resolve, reject) => {
        userModel
            .findById(userId)
            .populate('attendee')
            .then((result) => {
                resolve(result.attendee);
            }).catch((err) => reject(err));
    });
};

exports.findDonations = async (userId) => {
    return new Promise((resolve, reject) => {
        userModel
            .findById(userId)
            .populate('attendee.donations')
            .populate('attendee.donations.by')
            .then((result) => {
                resolve(result.attendee.donations);
            }).catch((err) => reject(err));
    });
};
