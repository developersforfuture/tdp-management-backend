const mongoose = require('../../common/services/mongoose.service').mongoose;
const Schema = mongoose.Schema;
const tourPoint = require('./tour_point.model');

const tourSegmentSchema = new Schema({
    start: tourPoint.Schema,
    end: tourPoint.Schema,
    name: {type: String, required: true},
    date: {type: Date, default: Date.now},
    createdAt: {type: Date, default: Date.now},
}, {versionKey: false});

tourSegmentSchema.virtual('id').get(function() {
    return this._id.toHexString();
});

// Ensure virtual fields are serialised.
tourSegmentSchema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: function(doc, ret) {
        delete ret._id;
        delete ret.hash;
    },
});
tourSegmentSchema.pre('save', (next) => {
    now = new Date();
    if (!this.createdAt) {
        this.createdAt = now;
    }
    return next();
});
tourSegmentSchema.findById = function(cb) {
    return this.model('TourSegment').find({id: this.id}, cb);
};

// Exports the country for use elsewhere.
module.exports.Model = mongoose.model('TourSegment', tourSegmentSchema);
module.exports.Schema = tourSegmentSchema;
