const UserService = require('./users.service');

exports.getAll = (req, res, next) => {
    const limit = req.query.limit &&
        req.query.limit <= 100 ? parseInt(req.query.limit) : 10;
    let page = 0;
    if (req.query) {
        if (req.query.page) {
            req.query.page = parseInt(req.query.page);
            page = Number.isInteger(req.query.page) ? req.query.page : 0;
        }
    }
    return UserService.list(limit, page)
        .then((result) => {
            return res.status(200).send(result);
        }).catch(((err) => next(err)));
};


exports.update = (req, res, next) => {
    if (req.body.password) {
        delete req.body.password;
    }
    UserService.patchUser(req.params.userId, req.body)
        .then((result) => {
            res.status(204).send();
        }).catch(((err) => next(err)));
};

exports.get = (req, res, next) => {
    return UserService.findById(req.params.id)
        .then((result) => {
            return res.status(200).send(result);
        }).catch(((err) => next(err)));
};

exports._delete = (req, res, next) => {
    UserService.removeById(req.params.userId)
        .then((result)=>{
            return res.status(204).send({});
        }).catch(((err) => next(err)));
};
