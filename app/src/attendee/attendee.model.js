const mongoose = require('../common/services/mongoose.service').mongoose;
const Schema = mongoose.Schema;
const attendeeDonation = require('./attendee_donation.model');
const attendeeSchema = new Schema({
    isBuildOwnTour: {type: Boolean, default: false},
    isJoinOtherTour: {type: Boolean, default: false},
    isPress: {type: Boolean, default: false},
    isOrga: {type: Boolean, default: false},
    home: {type: Object, default: {
        longitude: 0,
        latitude: 0,
        name: '',
        date: '',
    }},
    daysToParticipate: {type: Number, default: 0},
    distanceToParticipate: {type: Number, default: 0},
    meanOfTransportation: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'MeanOfTransportation',
        required: false,
    },
    unit: {type: String, default: 'km'},
    firstName: {type: String, default: ''},
    secondName: {type: String, default: ''},
    organisation: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Organisation',
        required: false,
    },
    country: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Country',
        required: false,
    },
    createdAt: {type: Date, default: Date.now},
    donations: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'AttendeeDonation',
        required: false,
    }],
    tours: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Tour',
        required: false,
    }]
});
attendeeSchema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: function(doc, ret) {
        delete ret._id;
        delete ret.hash;
    },
});

attendeeSchema.pre('save', (next) => {
    now = new Date();
    if (!this.createdAt) {
        this.createdAt = now;
    }
    return next();
});
exports.schema = attendeeSchema;
exports.Model = mongoose.model('Attendee', attendeeSchema);
