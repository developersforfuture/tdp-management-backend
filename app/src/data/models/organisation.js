const mongoose = require('../../common/services/mongoose.service').mongoose;
const Schema = mongoose.Schema;

const organisationSchema = new Schema({
    name: {type: String, required: true, unique: true},
    short_name: {type: String, required: true, unique: true},
    createdAt: {type: Date, default: Date.now},
}, {versionKey: false});

organisationSchema.virtual('id').get(function() {
    return this._id.toHexString();
});

// Ensure virtual fields are serialised.
organisationSchema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: function(doc, ret) {
        delete ret._id;
        delete ret.hash;
    },
}, {versionKey: false});
organisationSchema.pre('save', (next) => {
    now = new Date();
    if (!this.createdAt) {
        this.createdAt = now;
    }
    return next();
});
organisationSchema.findById = function(cb) {
    return this.model('Organisation').find({id: this.id}, cb);
};

// Exports the Organisation for use elsewhere.
module.exports = mongoose.model('Organisation', organisationSchema);
