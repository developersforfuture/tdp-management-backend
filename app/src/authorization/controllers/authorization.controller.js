const jwtSecret = require('../../common/config/env.config.js').jwt_secret;
const jwt = require('jsonwebtoken');
const UserService = require('./../../users/users.service');
const helper = require('./../services/helper.service');
const logger = require('./../../logger');
const nodemailer = require('nodemailer');

const transporter = nodemailer.createTransport({
    host: 'smtp.tourdeplanet.org',
    port: 465,
    auth: {
        user: 'verify@tourdeplanet.org',
        pass: process.env.MAILER_PASSWORD,
    },
    authMethod: 'login',
    secure: true,
    // localAddress: 'tourdeplanet.org',
    tls: {
        rejectUnauthorized: false,
    },
});
exports.login = (req, res, next) => {
    if (!req.body.userId) {
        return res.status(404).send({ errors: 'No user found' });
    }
    const userId = req.body.userId;
    const user = req.body.user;

    try {
        const refreshId = userId + jwtSecret;
        const hash = helper.saltHashPassword(refreshId);
        const userValues = {
            userId: userId,
            username: user.username,
            attendee: user.attendee,
            permissionLevel: user.permissionLevel,
            email: user.email,
        };
        const accessToken = jwt.sign(userValues, jwtSecret);
        const b = new Buffer(hash);
        const refreshToken = b.toString('hex');

        return res.status(201).send({ accessToken, refreshToken, userValues, userId });
    } catch (err) {
        return next(err);
    }
};

exports.refreshToken = (req, res, next) => {
    try {
        req.body = req.jwt;
        const token = jwt.sign(req.body, jwtSecret);
        return res.status(201).send({ id: token });
    } catch (err) {
        return res.status(500).send({ errors: err });
    }
};

exports.verifyRegistration = async(req, res, next) => {
    if (!req.body.token) {
        return res.status(404).send();
    }

    UserService.findByVerificationToken(req.body.token, (err, user) => {
        if (err) {
            logger.error('Find by token ' + req.body.token + ' ends in error:')
            logger.error(err);
            return res.status(404).send();
        }

        if (!user || null === user) {
            logger.warn('Find by token ' + req.body.token + ' ends in no user.');
            return res.status(404).send();
        }

        user.enabled = true;
        user.verification_date = new Date();
        user.save((err, data) => {
            if (err) {
                return res.status(400).send();
            }

            return res.status(200).send();
        });
    });
};

exports.register = (req, res, next) => {
    const token = jwt.sign({
            username: req.body.username,
            email: req.body.email,
            createdAt: new Date(),
        },
        jwtSecret,
    );

    return transporter.sendMail({
        to: req.body.email,
        from: 'verify@tourdeplanet.org',
        subject: 'Tour de Planet Register Verification',
        text: 'Thank you for registering Tour de Planet management application.  \n ' +
            'You are one step away from your climate contribution. Just verify your action by clicking the ' +
            'following link: \n\n https://management.tourdeplanet.org/auth/verify/' + token,
        html: '<h2>Thank you for registering Tour de Planet management application</h2><br />' +
            '<p>You are one step away from your climate contribution. Just verify your action by clicking the' +
            'following link: <br /><br />' +
            ' <a href="https://management.tourdeplanet.org/auth/verify/' + token + '">' +
            'https://management.tourdeplanet.org/auth/verify/' + token +
            '</a></p>',
    }, (err, info) => {
        if (err) {
            logger.warn('Error while sending the verification mail');
            logger.warn(err);
            return res.status(500).send('Problems to send verification mail. Please try again later');
        }
        logger.info('email send: ');
        logger.info(info);
        const user = {
            username: req.body.username,
            email: req.body.email,
            password: helper.saltHashPassword(req.body.plainPassword),
            newsletter_accepted: !!(req.body.hasOwnProperty('newsletterAccepted') &&
                req.body.newsletterAccepted),
            terms_accepted: !!(req.body.hasOwnProperty('termsAccepted') &&
                req.body.termsAccepted),
            enabled: false,
            verification_token: token,
            permissionLevel: 1,
        };
        UserService.createUser(user)
            .then((result) => {
                return res.status(201).send();
            }).catch(((err) => next(err)));
    });
};