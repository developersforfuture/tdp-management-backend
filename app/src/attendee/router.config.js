const permissionMiddleware = require(
    '../common/middlewares/auth.permission.middleware'
);
const validationMiddleware = require(
    '../common/middlewares/auth.validation.middleware'
);
const config = require('../common/config/env.config');
const AttendeeController = require('./../attendee/attendee.controller');
const attendeeValidationMiddleWare = require('./attendee.validation.middlewar')

exports.routesConfig = function(app) {
    app.get('/users/:userId/attendee-tour-profile', [
        validationMiddleware.validJWTNeeded,
        permissionMiddleware.userStillEnabled,
        permissionMiddleware
            .minimumPermissionLevelRequired(
                config.permissionLevels.NORMAL_USER,
            ),
        AttendeeController.getAttendeeTourProfile,

    ]);
    app.get('/users/:userId/attendee-profile', [
        validationMiddleware.validJWTNeeded,
        permissionMiddleware.userStillEnabled,
        permissionMiddleware
            .minimumPermissionLevelRequired(
                config.permissionLevels.NORMAL_USER,
            ),
        AttendeeController.getAttendeeProfile,
    ]);

    app.patch('/users/:userId/attendee-tour-profile', [
        validationMiddleware.validJWTNeeded,
        permissionMiddleware.userStillEnabled,
        permissionMiddleware
            .minimumPermissionLevelRequired(
                config.permissionLevels.NORMAL_USER,
            ),
        AttendeeController.updateAttendeeTourProfile,
    ]);
    app.patch('/users/:userId/attendee-profile', [
        validationMiddleware.validJWTNeeded,
        permissionMiddleware.userStillEnabled,
        permissionMiddleware
            .minimumPermissionLevelRequired(
                config.permissionLevels.NORMAL_USER,
            ),
        AttendeeController.updateAttendeeProfile,
    ]);
    app.patch('/users/:userId/attendee-password', [
        validationMiddleware.validJWTNeeded,
        permissionMiddleware.userStillEnabled,
        permissionMiddleware
            .minimumPermissionLevelRequired(
                config.permissionLevels.NORMAL_USER,
            ),
        AttendeeController.updateAttendeePassword,
    ]);
    app.get('/users/:userId/attendee-donations', [
        validationMiddleware.validJWTNeeded,
        permissionMiddleware.userStillEnabled,
        permissionMiddleware
            .minimumPermissionLevelRequired(
                config.permissionLevels.NORMAL_USER,
            ),
        AttendeeController.getAttendeeDonations,

    ]);
    app.post('/users/:userId/attendee-donations', [
        validationMiddleware.validJWTNeeded,
        permissionMiddleware.userStillEnabled,
        permissionMiddleware
            .minimumPermissionLevelRequired(
                config.permissionLevels.NORMAL_USER,
            ),
        attendeeValidationMiddleWare.hasRequiredFieldsForAddingDonation,
        AttendeeController.addAttendeeDonation,
    ]);
};
