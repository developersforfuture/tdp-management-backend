const mongoose = require('../../common/services/mongoose.service').mongoose;
const Schema = mongoose.Schema;
const tourPoint = require('./tour_point.model');
const organizer = require('./tour_organizer.model');
const segments = require('./tour_segment.model');
const meanOfTransportation = require('./../../data/models/meansoftransportation');

const tourSchema = new Schema({
    start: tourPoint.Schema,
    end: tourPoint.Schema,
    organizer: organizer.Schema,
    description: {type: String, required: false},
    type: {type: Schema.ObjectId, ref: meanOfTransportation},
    title: {type: String, required: true},
    date: {type: Date, default: Date.now},
    segments: [{type: Schema.ObjectId, ref: segments.Schema}],
    createdAt: {type: Date, default: Date.now},
}, {versionKey: false});

tourSchema.virtual('id').get(function() {
    return this._id.toHexString();
});

// Ensure virtual fields are serialised.
tourSchema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: function(doc, ret) {
        delete ret._id;
        delete ret.hash;
    },
});
tourSchema.pre('save', (next) => {
    now = new Date();
    if (!this.createdAt) {
        this.createdAt = now;
    }
    return next();
});
tourSchema.findById = function(cb) {
    return this.model('Tour').find({id: this.id}, cb);
};

// Exports the country for use elsewhere.
module.exports.Model = mongoose.model('Tour', tourSchema);
module.exports.Schema = tourSchema;
