const User = require('./users.model');
const attendee = require('./../attendee/attendee.model');
const logger = require('./../logger');

exports.findBymail = (email) => {
    return User.find({email: email});
};

exports.findByUsername = (username) => {
    return User.find({username: username});
};
exports.findByVerificationToken = (token) => {
    return User.find({token});
}
exports.findById = (id) => {
    return User.findById(id)
        .then((result) => {
            result = result.toJSON();
            delete result._id;
            delete result.__v;
            return result;
        });
};

exports.createUser = (userData) => {
    const user = new User({
        ...userData,
        attendee: new attendee.Model(
            userData.hasOwnProperty('attendee') ? userData.attendee : {},
        ),
    });
    return user.save();
};

exports.list = (perPage, page) => {
    return new Promise((resolve, reject) => {
        User.find()
            .limit(perPage)
            .skip(perPage * page)
            .exec(function(err, users) {
                if (err) {
                    reject(err);
                } else {
                    resolve(users);
                }
            });
    });
};

exports.findByVerificationToken = (token, cb) => {
    User.findOne({verification_token: token}, cb);
};

exports.patchUserAttendeeTourProfile = async (userId, attendeeData) => {
    return new Promise((resolve, reject) => {
        User.findById(userId, (err, user) => {
            if (err) throw new Error('No user found for id to update its atendee tour profile.');
            [
                'isBuildOwnTour',
                'isJoinOtherTour',
                'isPress',
                'isOrga',
                'home',
                'daysToParticipate',
                'distanceToParticipate',
                'meanOfTransportation',
                'unit',
            ].forEach((key) => {
                if (attendeeData.hasOwnProperty(key)) {
                    user.attendee[key] = attendeeData[key];
                } else {
                    logger.warn('Unknown property "' + key + '" on patch data when updating attendee tour profile.');
                }
            });
            user.save((err, user) => {
                if (err) reject(err);
                resolve(user);
            });
        });
    });
};

exports.patchUserAttendeeProfile = async (userId, attendeeData) => {
    return new Promise((resolve, reject) => {
        User.findById(userId, (err, user) => {
            if (err) throw new Error('No user found for id to update its atendee tour profile.');
            [
                'username',
                'mail',
                'firstName',
                'secondName',
                'organisation',
                'country',
            ].forEach((key) => {
                if (attendeeData.hasOwnProperty(key)) {
                    user.attendee[key] = attendeeData[key];
                } else {
                    logger.warn('Unknown property "' +
                    key +
                    '" on patch data when updating attendee tour profile.');
                }
            });
            [
                'username',
                'mail',
            ].forEach((key) => {
                if (attendeeData.hasOwnProperty(key)) {
                    if (key === 'mail') {
                        user.email = attendeeData[key];
                    } else {
                        user[key] = attendeeData[key];
                    }
                } else {
                    logger.warn('Unknown property "' +
                    key +
                    '" on patch data when updating attendee tour profile.');
                }
            });
            user.save((err, user) => {
                if (err) reject(err);
                resolve(user);
            });
        });
    });
};

exports.patchUserAttendeeDonations = async (userId, attendeeData) => {
    return new Promise((resolve, reject) => {
        User.findById(userId, (err, user) => {
            if (err) reject(new Error('No user found for id to update its atendee tour profile.'));
            if (!attendeeData.hasOwnProperty('donations')) reject(new Error('You can not patch donations and forgot to pass them'));
            user.attendee.donations = attendeeData.donations;
            user.save((err, user) => {
                if (err) reject(err);
                resolve(user);
            });
        });
    });
};

exports.removeById = (userId) => {
    return new Promise((resolve, reject) => {
        User.remove({_id: userId}, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve(err);
            }
        });
    });
};
