/* eslint-disable no-invalid-this */
process.env.NODE_ENV = 'test';

const User = require('../../users/users.model');
const MeansOfTransportation = require('./../../data/models/meansoftransportation');
const Tour = require('./../../tour/models/tour.model').Model;
const apiTestHelper = require('../api_test.helper');
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../../index');
const validMot = require('./data/mot.data').validMot;
const validTour = require('./data/tour.data').validTour;

chai.use(chaiHttp);

describe('Tour data of Attendee', async () => {
    before(async () => {
        User.deleteMany({}, (err) => {
            if (err) throw (err);
        });

        const userValues = await  apiTestHelper.registerUser('testuser', 'pAssW0rd!', 'test@tdp.de');
        this.token = userValues.token;
        this.userId = userValues.userId;

        const secondUserValues = await  apiTestHelper.registerUser('testuser', 'pAssW0rd!', 'test@tdp.de');
        this.secondToken = secondUserValues.token;
        this.secondUserId = secondUserValues.userId;

        const meanOfTransportation = new MeansOfTransportation(validMot);
        const firstMeanOfTransportation = await meanOfTransportation.save();
        this.firstMeanOfTransportationId = firstMeanOfTransportation.id;
    });

    describe('Add Tour', async () => {
        it('should break on missing title', async () => {
            const res = await chai.request(server)
                .post('/users/' + this.userId + '/tours')
                .set('authorization', 'Bearer ' + this.token)
                .set('Content-Type', 'application/json')
                .send({
                    organizer: {
                        name: 'Tour Organizer',
                        type: 'single'
                    },
                    start: {
                        name: 'Start',
                        longitude: 0.1,
                        latitude: 0.1,
                        date: '09/07/2020'
                    },
                    end: {
                        name: 'End',
                        longitude: 0.2,
                        latitude: 0.2,
                        date: '10/07/2020'
                    },
                    description: 'some description'
                });
            res.should.have.status(400);
        });
        it('should break on missing start', async () => {
            const res = await chai.request(server)
                .post('/users/' + this.userId + '/tours')
                .set('authorization', 'Bearer ' + this.token)
                .set('Content-Type', 'application/json')
                .send({
                    title: 'New tour',
                    organizer: {
                        name: 'Tour Organizer',
                        type: 'single'
                    },
                    end: {
                        name: 'End',
                        longitude: 0.2,
                        latitude: 0.2,
                        date: '10/07/2020',
                    },
                    description: 'some description'
                });
            res.should.have.status(400);
        });
        it('should break on missing end', async () => {
            const res = await chai.request(server)
                .post('/users/' + this.userId + '/tours')
                .set('authorization', 'Bearer ' + this.token)
                .set('Content-Type', 'application/json')
                .send({
                    title: 'New tour',
                    organizer: {
                        name: 'Tour Organizer',
                        type: 'single'
                    },
                    start: {
                        name: 'Start',
                        longitude: 0.1,
                        latitude: 0.1,
                        date: '09/07/2020'
                    },
                    description: 'some description'
                });
            res.should.have.status(400);
        });
        it('should break on missing organizer', async () => {
            const res = await chai.request(server)
                .post('/users/' + this.userId + '/tours')
                .set('authorization', 'Bearer ' + this.token)
                .set('Content-Type', 'application/json')
                .send({
                    title: 'New tour',
                    start: {
                        name: 'Start',
                        longitude: 0.1,
                        latitude: 0.1,
                        date: '09/07/2020'
                    },
                    end: {
                        name: 'End',
                        longitude: 0.2,
                        latitude: 0.2,
                        date: '10/07/2020',
                    },
                    description: 'some description'
                });
            res.should.have.status(400);
        });
        it('should accept valid tour data and return the newly created tour', async () => {
            const res = await chai.request(server)
                .post('/users/' + this.userId + '/tours')
                .set('authorization', 'Bearer ' + this.token)
                .set('Content-Type', 'application/json')
                .send(validTour);
            res.should.have.status(201);
            res.body.should.have.property('title', 'Tour', 'The result\'s title should be there');
            res.body.should.have.property('description', 'some description', 'The result\'s description should be there');
            res.body.should.have.property('start');
            res.body.start.should.have.property('name', 'Start', 'Results Start point should have a name');
            res.body.start.should.have.property('latitude', 0.1, 'Results Start point should have a latitude of "0.1"');
            res.body.start.should.have.property('longitude', 0.1, 'Results Start point should have a longitude of "0.1"');
            res.body.start.should.have.property('date');
            res.body.should.have.property('end');
            res.body.end.should.have.property('name', 'End', 'Results End point should have a name');
            res.body.end.should.have.property('latitude', 0.2, 'Results End point should have a latitude of "0.2"');
            res.body.end.should.have.property('longitude', 0.2, 'Results End point should have a longitude of "0.2"');
            res.body.end.should.have.property('date');
            res.body.should.have.property('organizer');
            res.body.organizer.should.have.property('name', 'Tour Organizer', 'Results organizer should have a name');
            res.body.organizer.should.have.property('type', 'single', 'Results organizer should have a type');
        });
    });

    describe('Get single Tour', () => {
        before(async () => {
            const res = await chai.request(server)
                .post('/users/' + this.userId + '/tours')
                .set('authorization', 'Bearer ' + this.token)
                .set('Content-Type', 'application/json')
                .send(validTour);
            this.firstTour = res.body;
        });

        it('should return the own tour', async () => {
            const res = await chai.request(server)
                .get('/users/' + this.userId + '/tours/' + this.firstTour.id)
                .set('authorization', 'Bearer ' + this.token)
                .set('Content-Type', 'application/json')

            res.should.have.status(200);
            res.body.should.have.property('title', 'Tour', 'The result\'s title should be there');
            res.body.should.have.property('description', 'some description', 'The result\'s description should be there');
            res.body.should.have.property('start');
            res.body.start.should.have.property('name', 'Start', 'Results Start point should have a name');
            res.body.start.should.have.property('latitude', 0.1, 'Results Start point should have a latitude of "0.1"');
            res.body.start.should.have.property('longitude', 0.1, 'Results Start point should have a longitude of "0.1"');
            res.body.start.should.have.property('date');
            res.body.should.have.property('end');
            res.body.end.should.have.property('name', 'End', 'Results End point should have a name');
            res.body.end.should.have.property('latitude', 0.2, 'Results End point should have a latitude of "0.1"');
            res.body.end.should.have.property('longitude', 0.2, 'Results End point should have a longitude of "0.1"');
            res.body.end.should.have.property('date');
            res.body.should.have.property('organizer');
            res.body.organizer.should.have.property('name', 'Tour Organizer', 'Results organizer should have a name');
            res.body.organizer.should.have.property('type', 'single', 'Results organizer should have a type');
        });


        it('should not return tour of other user the own tour', async () => {
            const res = await chai.request(server)
                .get('/users/' + this.secondUserId + '/tours/' + this.firstTour.id)
                .set('authorization', 'Bearer ' + this.secondToken)
                .set('Content-Type', 'application/json')

            res.should.have.status(404);
        });

    });
    describe('Get Tour list', () => {});
    describe('Patch tour data', () => {});
    describe('Add tour segment', () => {});
});
