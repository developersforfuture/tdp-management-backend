const mongoose = require('../common/services/mongoose.service').mongoose;
const Schema = mongoose.Schema;
const attendee = require('./../attendee/attendee.model');

const userSchema = new Schema({
    attendee: attendee.schema,
    username: { type: String, unique: true, required: true },
    email: { type: String, unique: true, required: true },
    terms_accepted: { type: String, required: true },
    newsletter_accepted: { type: Boolean, required: true },
    verification_token: { type: String, required: true },
    verirfication_date: { type: Date, default: null },
    permissionLevel: { type: Number, required: true, default: 1 },
    password: String,
    locale: String,
    private_key: String,
    api_key: String,
    locked: Boolean,
    enabled: Boolean,
    password_reset_token: String,
    password_reset_token_emails_sent: String,
    user_roles: Array,
    user_groups: Array,
    user_settings: Array,
    createdAt: { type: Date, default: Date.now },
}, { versionKey: false });

userSchema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: function(doc, ret) {
        delete ret._id;
        delete ret.hash;
    },
});

userSchema.pre('save', (next) => {
    now = new Date();
    if (!this.createdAt) {
        this.createdAt = now;
    }
    return next();
});

module.exports = mongoose.model('User', userSchema);