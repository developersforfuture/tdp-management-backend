const mongoose = require('../../common/services/mongoose.service').mongoose;
const Schema = mongoose.Schema;

const meanoftransportationSchema = new Schema({
    name: {type: String, required: true, unique: true},
    iconClass: {type: String, required: true, unique: true},
    color: {type: String, required: true, unique: true},
    createdAt: {type: Date, default: Date.now},
}, {versionKey: false});

meanoftransportationSchema.virtual('id').get(function() {
    return this._id.toHexString();
});

// Ensure virtual fields are serialised.
meanoftransportationSchema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: function(doc, ret) {
        delete ret._id;
        delete ret.hash;
    },
});
meanoftransportationSchema.pre('save', (next) => {
    now = new Date();
    if (!this.createdAt) {
        this.createdAt = now;
    }
    return next();
});

meanoftransportationSchema.findById = function(cb) {
    return this.model('MeanOfTransportation').find({id: this.id}, cb);
};

// Exports the meanoftransportation for use elsewhere.
module.exports = mongoose
    .model('MeanOfTransportation', meanoftransportationSchema);
