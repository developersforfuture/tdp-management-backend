# Tour de Planet Backend Application

- a thin wrapper in front of mongoDB

## Usate

```bash
git clone git@gitlab.com:developersforfuture/tdp-management-backend.git
cd app/src
docker-compose up -d
npm install
NODE_ENV=dev npm run start
```

## Tests

```bash
cd app/src
npm test
```
