const TourModel = require('./../models/tour.model').Model;
const userModel = require('../../users/users.model');
const attendeesService = require('../../attendee/attendee.service');

exports.findById = (id) => {
    return TourModel.findById(id)
        .then((result) => {
            result = result.toJSON();
            delete result._id;
            delete result.__v;
            return result;
        });
};

exports.createTourForUser = (tourData, userId) => {
    return new Promise((resolve, reject) => {
        userModel.findById(userId)
            .populate('user.attendee')
            .exec()
            .then((user) => {
                if (null === user) {
                    return reject('No user found with id: '.userId);
                }
                const tour = new TourModel(tourData);
                return tour.save((err, tour) => {
                    if (err) {
                        return reject('no tour created: '.err);
                    }
                    user.attendee.tours.push(tour);
                    return user.save((err, user) => {
                        if (err) {
                            return reject('unable to persist tour on user');
                        }
                        return resolve(tour);
                    });
                });
            }).catch((err) => {
                reject('issues to get user by id ' + userId);
        });
    })
};
exports.getListByUser = (userId) => {
    return attendeesService
        .findAttendeeByUserId(userId)
        .then((attendee) => {
            return new  Promise((resolve, reject) => {
                if (null === attendee || !attendee.hasOwnProperty('tours')) {
                    return reject('No user found with id: '.userId);
                }

                return resolve(attendee.tours);
            })
        });
}

exports.getTourByIdAndUser = (userId, tourId) => {
    this.getListByUser(userId)
        .then((tours) => {
            return new Promise((resolve, reject) => {
                if (!tours || tours.length === 0) {
                    return reject('No tour found for user '+userId+' and tour id '+tourId);
                }

                const t = tours.filter((ft) => ft.id === tourId);
                if (t.length !== 1) {
                    return reject('No tour found for user '+userId+' and tour id '+tourId);
                }

                return resolve(t.pop());
            })
        });
}

exports.listAll = (perPage, page) => {
    return new Promise((resolve, reject) => {
        TourModel.find()
            .limit(perPage)
            .skip(perPage * page)
            .exec(function(err, users) {
                if (err) {
                    reject(err);
                } else {
                    resolve(users);
                }
            });
    });
};

exports.patchTour = (id, tourData) => {
    return new Promise((resolve, reject) => {
        TourModel.findById(id, function(err, tour) {
            if (err) reject(err);
            for (const i in tourData) {
                if (tourData.hasOwnProperty(i)) {
                    tour[i] = tourData[i];
                }
            }
            tour.save(function(err, updatedTour) {
                if (err) return reject(err);
                resolve(updatedTour);
            });
        });
    });
};

/*
 * Update some values of a tour.
 *
 * @param id
 * @param userData
 * @returns {Promise<Object>}
 */
exports.patchTour = (id, userData) => {
    return new Promise((resolve, reject) => {
        TourModel.findById(id, function(err, tour) {
            if (err) reject(err);
            for (const i in userData) {
                if (userData.hasOwnProperty(i)) {
                    tour[i] = userData[i];
                }
            }
            tour.save(function(err, updatedUser) {
                if (err) return reject(err);
                resolve(updatedUser);
            });
        });
    });
};

exports.addTourSegment = (tour, segmentData) => {

};

exports.removeTourSegment = (tour, segmentId) => {

};

exports.removeById = (userId) => {
    return new Promise((resolve, reject) => {
        TourModel.remove({_id: userId}, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve(err);
            }
        });
    });
};


exports.findToursByUserId = async (userId) => {
    return new Promise((resolve, reject) => {
        userModel
            .findById(userId)
            .populate('attendee.tours')
            .then((result) => {
                resolve(result.attendee.tours);
            }).catch((err) => reject(err));
    });
};