const mongoose = require('../../common/services/mongoose.service').mongoose;
const Schema = mongoose.Schema;

const tourPointSchema = new Schema({
    longitude: {type: Number, required: false},
    name: {type: String, required: true, unique: true},
    latitude: {type: Number, required: true},
    date: {type: Date, default: Date.now},
    createdAt: {type: Date, default: Date.now},
}, {versionKey: false});

tourPointSchema.virtual('id').get(function() {
    return this._id.toHexString();
});

// Ensure virtual fields are serialised.
tourPointSchema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: function(doc, ret) {
        delete ret._id;
        delete ret.hash;
    },
});
tourPointSchema.pre('save', (next) => {
    now = new Date();
    if (!this.createdAt) {
        this.createdAt = now;
    }
    return next();
});
tourPointSchema.findById = function(cb) {
    return this.model('TourPoint').find({id: this.id}, cb);
};

// Exports the country for use elsewhere.
module.exports.Model = mongoose.model('TourPoint', tourPointSchema);
module.exports.Schema = tourPointSchema;
