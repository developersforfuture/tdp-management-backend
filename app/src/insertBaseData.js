const organisation = require('./data/models/organisation');
const country = require('./data/models/country');
const meansoftransportation = require('./data/models/meansoftransportation');
const logger = require('./logger');
const attendeeDonation = require('./attendee/attendee.model').Model;
const userModel = require('./users/users.model');
attendeeDonation.deleteMany({}, (err, result) => {
    if (err) logger.error(err);
    logger.info(result);
});
userModel.deleteMany({}, (err, result) => {
    if (err) logger.error(err);
    logger.info(result);
});

organisation.deleteMany({}, (err, result) => {
    if (err) logger.error(err);
    logger.info(result);
    organisation.insertMany([
        {
            name: 'Fridays For Future',
            short_name: 'FFF',
        },
        {
            name: 'Extinction Rebelion',
            short_name: 'XR',
        },
        {
            name: 'Parents For Future',
            short_name: 'P4F',
        },
        {
            name: 'Developers For Future',
            short_name: 'D4F',
        },
        {
            name: 'Keine Organisation',
            short_name: 'NO',
        },
    ], (err, result) => {
        if (err) logger.error(err);
        // logger.info(result);
    });
});


country.deleteMany({}, (err, result) => {
    if (err) logger.error(err);
    logger.info(result);
    country.insertMany([
        {
            name: 'Germany',
            short_name: 'de',
        },
        {
            name: 'Poland',
            short_name: 'pl',
        },
        {
            name: 'France',
            short_name: 'fr',
        },
        {
            name: 'United Kingdom',
            short_name: 'uk',
        },
        {
            name: 'Denmark',
            short_name: 'dk',
        },
        {
            name: 'Spain',
            short_name: 'es',
        },
        {
            name: 'Portugal',
            short_name: 'pt',
        },
    ], (err, result) => {
        if (err) logger.error(err);
       // logger.info(result);
    });
});

meansoftransportation.deleteMany({}, (err, result) => {
    if (err) logger.error(err);
    logger.info(result);
    meansoftransportation.insertMany([
        {
            name: 'Bike',
            iconClass: 'tdp_normal_bike',
            color: '#0d19ea',
        },
        {
            name: 'Hiking',
            iconClass: 'tdp_hiking_boot',
            color: '#373fd2',
        },
        {
            name: 'Running',
            iconClass: 'tdp_running_shoe',
            color: '#4670fc',
        },
        {
            name: 'Canoe/Kayak',
            iconClass: 'tdp_boat',
            color: '#7087ca',
        },
        {
            name: 'Racing Bike',
            iconClass: 'tdp_race_bike',
            color: '#211f9c',
        },
        {
            name: 'Skateboard',
            iconClass: 'tdp_race_bike',
            color: '#7087ca',
        },
        {
            name: 'Horse',
            iconClass: 'tdp_horse',
            color: '#88888f',
        },
    ], (err, result) => {
        if (err) logger.error(err);
        // logger.info(result);
    });
});

