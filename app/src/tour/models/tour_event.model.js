const mongoose = require('../../common/services/mongoose.service').mongoose;
const Schema = mongoose.Schema;

const tourEventSchema = new Schema({
    point: {},
    author: {type: String, required: true},
    title: {type: String, required: true},
    url: {type: String, required: false},
    name: {type: String, required: true, unique: true},
    type: {type: String, required: true},
    date_from: {type: Date, default: Date.now},
    date_to: {type: Date, default: Date.now},
    createdAt: {type: Date, default: Date.now},
}, {versionKey: false});

tourEventSchema.virtual('id').get(function() {
    return this._id.toHexString();
});

// Ensure virtual fields are serialised.
tourEventSchema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: function(doc, ret) {
        delete ret._id;
        delete ret.hash;
    },
});
tourEventSchema.pre('save', (next) => {
    now = new Date();
    if (!this.createdAt) {
        this.createdAt = now;
    }
    return next();
});
tourEventSchema.findById = function(cb) {
    return this.model('TourEvent').find({id: this.id}, cb);
};

// Exports the country for use elsewhere.
module.exports.Model = mongoose.model('TourEvent', tourEventSchema);
module.exports.Schema = tourEventSchema;
