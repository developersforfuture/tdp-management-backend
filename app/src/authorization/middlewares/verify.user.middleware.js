/* eslint-disable require-jsdoc */
const UserService = require('../../users/users.service');
const helper = require('../services/helper.service');
const logger = require('./../../logger');

module.exports = {
    hasAuthValidFields,
    hasRegistrationValidFields,
    hasNoUsedUserNameAndEmail,
    isPasswordAndUserMatch,
};

async function hasAuthValidFields(req, res, next) {
    if (req.body) {
        const hasUsername = !!(req.body.username);
        if (!hasUsername) {
            return res
                .status(400)
                .send({error: 'Missing username.'});
        }
        if (!req.body.plainPassword) {
            return res
                .status(400)
                .send({error: 'Missing password field'});
        }
        return next();
    } else {
        return res
            .status(400)
            .send({errors: 'Missing mail and password fields'});
    }
};

async function hasRegistrationValidFields(req, res, next) {
    if (req.body) {
        if (!req.body.hasOwnProperty('email')) {
            return res.status(400).send({error: 'Missing mail field'});
        }
        if (!req.body.hasOwnProperty('username')) {
            return res.status(400).send({error: 'Missing username field'});
        }

        if (!req.body.hasOwnProperty('plainPassword')) {
            return res.status(400).send({error: 'Missing password field'});
        }

        if (!/[a-z]/.test(req.body.plainPassword)) {
            return res.status(400).send({error: 'Password should contain lower case'});
        }

        if (!/[A-Z]/.test(req.body.plainPassword)) {
            return res.status(400).send({error: 'Password should contain upper case'});
        }

        if (!/\d/.test(req.body.plainPassword)) {
            return res.status(400).send({error: 'Password should contain numbers'});
        }

        if (!/[@$!%*#?&]/.test(req.body.plainPassword)) {
            return res.status(400).send({error: 'Password should at leas one special character'});
        }

        if (req.body.plainPassword.length < 8) {
            return res.status(400).send({error: 'Password should have at least 8 characters.'});
        }

        if (!req.body.hasOwnProperty('termsAccepted') || !req.body.termsAccepted) {
            return res.status(400).send({error: 'You should accept the terms.'});
        }

        return next();
    } else {
        return res.status(400).send({errors: 'Missing mail and password fields'});
    }
};

async function hasNoUsedUserNameAndEmail(req, res, next) {
    const userByMail = await UserService.findBymail(req.body.email);
    if (0 !== userByMail.length) {
        return res
            .status(400)
            .send({error: 'Mail ' + req.body.email + ' addresss still exists'});
    }
    const userByUsername = await UserService.findByUsername(req.body.username);
    if (0 !== userByUsername.length) {
        return res
            .status(400)
            .send({error: 'Username ' + req.body.username + ' still exists'});
    }

    return next();
}

async function isPasswordAndUserMatch(req, res, next) {
    const userByMail = await UserService.findBymail(req.body.username);
    const userByUsername = await UserService.findByUsername(req.body.username);

    let user = null;
    if (0 !== userByMail.length) {
        user = userByMail;
    } else if (0 !== userByUsername.length) {
        user = userByUsername;
    }
    if (null === user) {
        logger.info('No user found for attemd with ' + req.body.username);
        return res.status(404).send({error: 'No user found.'});
    }

    const passwordFields = user[0].password.split('$');
    const salt = passwordFields[0];
    const passwordHash = passwordFields[1];
    const hash = helper.sha512(req.body.plainPassword, salt);
    if (hash === passwordHash) {
        req.body.userId = user[0]._id;
        req.body.user = user[0];
        return next();
    } else {
        return res.status(400).send({errors: ['Invalid e-mail or password']});
    }
};
