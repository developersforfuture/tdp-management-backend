const config = require('./common/config/env.config.js');

const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const morgan = require('morgan');
const AuthorizationRouter = require('./authorization/routes.config');
const UserRouter = require('./users/router.config');
const attendeeRouter = require('./attendee/router.config');
const tourRouter = require('./tour/router.config');
const logger = require('./logger');

app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', process.env.ALLOW_ORIGIN_CORS);
    res.header('Access-Control-Allow-Credentials', 'true');
    res.header(
        'Access-Control-Allow-Methods', 'GET,HEAD,PUT,PATCH,POST,DELETE',
    );
    res.header(
        'Access-Control-Expose-Headers',
        'Content-Length',
    );
    res.header(
        'Access-Control-Allow-Headers',
        'Accept, Authorization, Content-Type, X-Requested-With, Range',
    );
    if (req.method === 'OPTIONS') {
        return res.sendStatus(200);
    } else {
        return next();
    }
});

// parse application/json and look for raw text
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.text());
app.use(bodyParser.json({type: 'application/json'}));

AuthorizationRouter.routesConfig(app);
app.use('/data', require('./data/controllers/data.controller'));
UserRouter.routesConfig(app);
attendeeRouter.routesConfig(app);
tourRouter.routesConfig(app);
app.use(morgan('combined', {stream: logger.stream}));

app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' || req.app.get('env') === 'test' ? err : {};

    // add this line to include winston logging
    logger.error(
        `${err.status || 500} - ${err.message} - ${req.originalUrl} - ${req.method} - ${req.ip}`,
    );
    // render the error page
    return res.status(err.status || 500).send({error: err.message || 'Error'});
});

app.listen(config.port, () => {
    logger.info('app listening at port ' + config.port);
});

module.exports= app; // for testing
