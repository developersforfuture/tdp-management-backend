// During the test the env variable is set to test
process.env.NODE_ENV = 'test';

const mongoose = require('mongoose');
const Organisation = require('../../data/models/organisation');

// Require the dev-dependencies
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../../index');
const should = chai.should();

chai.use(chaiHttp);
// Our parent block
describe('Organisations', () => {
    beforeEach((done) => {
        Organisation.deleteMany({}, (err) => {
            if (err) throw Error(err);
            done();
        });
    });

    describe('GET /data/organisations (list organisations)', () => {
        beforeEach((done) => {
            const orga = new Organisation({name: 'Test Orga', short_name: 'ti'});
            orga.save((err, o) => {
                if (err) throw Error(err);
                done();
            });
        });

        it('should GET one organisation, when one is builded', (done) =>{
            chai.request(server)
                .get('/data/organisations')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.be.eql(1);
                    done();
                });
        });
    });
});
