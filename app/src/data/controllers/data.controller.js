/* eslint-disable require-jsdoc */
const organisation = require('./../models/organisation');
const MeanOfTransportation = require('./../models/meansoftransportation');
const Country = require('./../models/country');
const express = require('express');
const router = express.Router();
const donation = require('./../../attendee/attendee_donation.model').Model;
router.get('/organisations', getOrganizations);
router.get('/countries', getCountries);
router.get('/meansoftransportation', getMeansofTransportation);
router.get('/map', getMapData);

module.exports = router;

async function getMapData(req, res, next) {
    const donations = await donation.find().exec();
    const result = {donations, tours: [], events: []};
    return res.status(200).send(result);
};

function getOrganizations(req, res, next) {
    organisation.find().exec((err, result) => {
        if (err) return res.sendStatus(400);
        return res.status(200).send(result);
    });
}

async function getMeansofTransportation(req, res, next) {
    const mOT = await MeanOfTransportation.find().exec() || [];
    return res.status(200).send(mOT);
}

async function getCountries(req, res, next) {
    const countries = await Country.find().exec() || [];
    return res.status(200).send(countries);
}
