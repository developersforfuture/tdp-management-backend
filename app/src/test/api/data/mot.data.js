exports.validMot = {name: 'Bike', iconClass: 'tdp_test', color: 'blue'};
exports.invalidMots = [
    {name: 'Bike', iconClass: 'tdp_test'},
    {name: 'Bike', color: 'blue'},
    {iconClass: 'tdp_test', color: 'blue'}
];