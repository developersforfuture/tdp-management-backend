const jwt = require('jsonwebtoken');
const secret = require('../config/env.config.js').jwt_secret;
const helper = require('./../../authorization/services/helper.service');
const logger = require('./../../logger');

exports.verifyRefreshBodyField = (req, res, next) => {
    if (req.body && req.body.refreshToken) {
        return next();
    } else {
        logger.warn('Token expired, lets login in again for now');
        return res.status(401).send('Not authenticated, please login');
    }
};

exports.validRefreshNeeded = (req, res, next) => {
    const b = new Buffer(req.body.refreshToken, 'hex');
    const refreshToken = b.toString();
    const hash = helper.sha512(req.jwt.userId + secret, eq.jwt.refreshKey);
    if (hash === refreshToken) {
        req.body = req.jwt;
        return next();
    } else {
        logger.warn('Refresh Token not valid, lets login in again for now');
        return res.status(401).send('Not authenticated, please login');
    }
};


exports.validJWTNeeded = (req, res, next) => {
    if (req.headers['authorization']) {
        try {
            const authorization = req.headers['authorization'].split(' ');
            if (authorization[0] !== 'Bearer') {
                logger.warn('Request secured area with no token');
                return res.status(401).send('Not authenticated, please login');
            } else {
                req.jwt = jwt.verify(authorization[1], secret);
                return next();
            }
        } catch (err) {
            logger.warn('Error on auth request' + err);
            return res.status(401).send('Not authenticated, please login');
        }
    } else {
        return res.status(401).send('Not authenticated, please login');
    }
};