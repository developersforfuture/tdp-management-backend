const ADMIN_PERMISSION = 4096;
const logger = require('./../../logger');
const UserModel = require('./../../users/users.model');

exports.minimumPermissionLevelRequired = (requiredPermissionLevel) => {
    return (req, res, next) => {
        const userPermissionLevel = parseInt(req.jwt.permissionLevel);
        if (userPermissionLevel & requiredPermissionLevel) {
            return next();
        } else {
            logger.warn('minimum permissions not reached');
            return res.status(403).send({ error: 'No required Permissions' });
        }
    };
};

exports.onlySameUserOrAdminCanDoThisAction = (req, res, next) => {
    const userPermissionLevel = parseInt(req.jwt.permissionLevel);
    const userId = req.jwt.userId;
    if (req.params && req.params.userId && userId === req.params.userId) {
        return next();
    } else {
        if (userPermissionLevel & ADMIN_PERMISSION) {
            return next();
        } else {
            logger.warn('admin permissions not reached');
            return res
                .status(403)
                .send({ error: 'not same level, or non matching ids.' });
        }
    }
};

exports.sameUserCantDoThisAction = (req, res, next) => {
    const userId = req.jwt.userId;

    if (req.params.userId !== userId) {
        return next();
    } else {
        logger.warn('Not my data');
        return res.status(403).send();
    }
};
exports.userStillEnabled = async(req, res, next) => {
    const userId = req.jwt.userId;
    const user = await UserModel.findById(userId);
    if (!user || !user.enabled) {
        logger.warn('Loged out caused by invalid user:');
        return res.status(401).send('Not authenticated, please login');
    }

    return next();
};