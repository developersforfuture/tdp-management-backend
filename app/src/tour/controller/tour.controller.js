const tourService = require('../services/tour.service');
const logger = require('../../logger');

/**
 * Builds a single tour response of a given and registered user
 */
exports.getTour = (req, res, next) => {
    if (!req.params.hasOwnProperty('tourId')) {
        logger.warn('No tour id found');
        return res.status(404).send();
    }

    return tourService.getTourByIdAndUser(req.params.userId, req.params.tourId)
        .then((result) => {
            res.status(200).send(result.shift());
        })
        .catch((err) => {
            logger.warn('Fetching a users tour (userId: '+req.params.userId+', tourId: '+req.params.tourId+') ends up in an error ', err);
            return res.status(400).send();
        });
};

/**
 * Builds a single tour response of a given and registered user
 */
exports.getTours = (req, res, next) => {
    return tourService.getListByUser(req.params.userId).then((list) => {
        return res.status(200).send(list);
    }).catch((err) => res.status(400).send(err));
};

/**
 * Patches parts of a tour of a given and registered user
 */
exports.patchTour = (req, res, next) => {
    if (!req.body.tourData) {
        logger.warn('No tour data found, when trying to patch them');
        return res.status(400).send();
    }

    return tourService.patchTour(req.body.tourData)
        .then((result) => {
            if (!result) {
                logger.warn('Problems to patch the tour data.');
                return res.status(400).send();
            }
            res.status(200).send(result.JSON());
        })
        .catch((err) => {
            logger.warn('Fetching a user ends up in an error', err);
            return res.status(400).send();
        });
};

/**
 * Creates a new tour of a given and registered user.
 */
exports.createTour = (req, res, next) => {
    if (!req.body.tourData) {
        logger.warn('No tour data found, when trying to create it.');
        return res.status(400).send();
    }
    if (!req.params.userId) {
        logger.warn('No user id found');
        return res.status(400).send();
    }

    return tourService.createTourForUser(req.body.tourData, req.params.userId).then((result) => {
        return res.status(201).send(result);
    }).catch((err) => {
        logger.warn('Issues to create a tour for an user ', err);
        return res.status(400).send(err);
    });
};

exports.addTourSegment = (req, res, next) => {
    if (!req.body.segementData) {
        logger.warn('No tour data found, when trying to patch them');
        return res.status(400).send();
    }

    return tourService.addTourSegment(req.body.segementData)
        .then((result) => {
            if (!result) {
                logger.warn('Problems to add tour segment.');
                return res.status(400).send();
            }
            res.status(201).send(result.JSON());
        })
        .catch((err) => {
            logger.warn('Adding a tour segment ends in an error', err);
            return res.status(400).send();
        });
};

exports.removeTourSegment = (req, res, next) => {
    if (!req.body.id
        || !req.params.hasOwnProperty('id')
        || req.body.id !== req.body.params.id
    ) {
        logger.warn('Segment Id invalid');
        return res.status(400).send();
    }

    return tourService.removeTourSegment(req.body.tour, req.body.id)
        .then((result) => {
            if (!result) {
                logger.warn('Problems to remove a segment.');
                return res.status(400).send();
            }
            res.status(201).send(result.JSON());
        })
        .catch((err) => {
            logger.warn('Removeing a tour segment ends in an error', err);
            return res.status(400).send();
        });
};
